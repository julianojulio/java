package example.greetings;

import examples.data.Data;

import java.util.List;
import java.util.ServiceLoader;

import static java.util.stream.Collectors.toList;

public class Greetings {

    public static void main(String[] args) {
        final List<Data> dataProviders = ServiceLoader.load(Data.class).stream()
                .map(ServiceLoader.Provider::get)
                .collect(toList());

        System.out.println("Hello " + dataProviders.get(0).value());
        System.out.println("Hello " + dataProviders.get(1).value());
    }
}
