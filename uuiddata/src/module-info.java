module examples.data.uuid {
    requires jdk.incubator.httpclient;
    requires transitive examples.data;

    provides examples.data.Data with examples.data.uuid.UuidGenerator;

    exports examples.data.uuid;
}