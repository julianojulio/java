package examples.improvements;

import org.junit.jupiter.api.Test;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class StreamImprovementsTest {

    @Test
    void testStreamImprovementsFilter() {
        System.err.print(
                IntStream.range(1, 100)
                        .filter(number -> {
                            System.out.print(number + " ");
                            return number <= 10;
                        })
                        .boxed()
                        .collect(toList()));
    }

    @Test
    void testStreamImprovementsDropWhile() {
        System.err.print(
                IntStream.range(1, 100)
                        .takeWhile(number -> {
                            System.out.print(number + " ");
                            return number <= 10;
                        })
                        .boxed()
                        .collect(toList()));
    }

    @Test
    void testDropTakeWhileUnordered() {
        final List<Integer> takeWhile = Stream.of(1, 3, 5, 6, 8, 6, 2, 18).takeWhile(no -> no <= 5).collect(toList());
        final List<Integer> dropWhile = Stream.of(1, 3, 5, 6, 8, 6, 2, 18).dropWhile(no -> no <= 5).collect(toList());

        System.out.println(takeWhile);
        System.out.println(dropWhile);
    }

    @Test
    void testStream() {
        final var evens = IntStream.rangeClosed(1, 100)
                .filter((@Max(50) int number) -> (number % 2) == 0)
                .boxed()
                .collect(toList());

        System.out.println(evens);
    }
}
