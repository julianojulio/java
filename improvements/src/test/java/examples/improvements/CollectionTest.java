package examples.improvements;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CollectionTest {

    @Test
    void testCopy() {
        var numbers = new Integer[]{1, 2};

        var asList = Arrays.asList(numbers);

        final var copy = List.copyOf(asList);
        final var unmodifiableList = Collections.unmodifiableList(asList);

        assertThat(asList).isEqualTo(copy).isEqualTo(unmodifiableList);
    }


}
