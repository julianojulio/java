package examples.improvements;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class OptionalImprovementTest {

    @Test
    void testOptionalOr() {
        final Optional<Integer> theAnswer = Optional.of(42);
        final Optional<Integer> empty = Optional.empty();

        assertThat(empty.or(() -> theAnswer))
                .isEqualTo(Optional.of(42))
                .contains(42);
    }

    @Test
    void testOptionalOrSupplier() {
        assertThat(new FlightService().getFlight("1")).isEqualTo("flight-1");
    }
}
