package examples.improvements;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;

public class TryResourcesTest {

    @Test
    void testTryWithResources() throws Exception {

        var br = new BufferedReader(new FileReader("src/test/resources/mcwie.txt"));

        try (br) {
            String word = br.lines().findFirst().orElseThrow();
            System.out.println("Most common word in English: " + word);
        }
    }
}
