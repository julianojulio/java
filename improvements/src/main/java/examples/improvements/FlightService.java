package examples.improvements;

import java.util.Optional;

public class FlightService {

    private final FlightRepositoryCache cache;
    private final FlightRepositoryGulfstream service;

    public FlightService() {
        this.cache = new FlightRepositoryCache();
        this.service = new FlightRepositoryGulfstream();
    }

    public String getFlight(final String id) {
        return cache.get(id)
                .or(() -> service.get(id))
                .orElseThrow();
    }

    public class FlightRepositoryCache implements FlightRepository {

        @Override
        public Optional<String> get(String id) {
            return Optional.empty();
        }
    }

    public class FlightRepositoryGulfstream implements FlightRepository {

        @Override
        public Optional<String> get(String id) {
            return Optional.of("flight-1");
        }
    }

}
