package examples.improvements;

import java.util.Optional;

public interface FlightRepository {

    Optional<String> get(final String id);
}
