# Java

## Compact Strings
Before Java 9
http://hg.openjdk.java.net/jdk7u/jdk7u6/jdk/file/8c2c5d63a17e/src/share/classes/java/lang/String.java#l111

Java 9
http://hg.openjdk.java.net/jdk9/client/jdk/file/a046521803b2/src/java.base/share/classes/java/lang/String.java#l138

## Jshell
Feedback: `/set feedback verbose`

## Stream takeWhile and dropWhile
 - https://javadeveloperzone.com/java-9/java-stream-takewhile-dropwhile-example/
    - `Stream.of(1,3,5,6,8,6,2,18).takeWhile(no->no<=5).forEach(System.out::println);`
    - `Stream.of(1,3,5,6,8,6,2,18).dropWhile(no->no<=5).forEach(System.out::println);`

## Modules
- Modules: https://blog.codefx.org/java/java-module-system-tutorial/
  - `requires` vs `requires transient`: https://stackoverflow.com/a/46504020
  - http://www.baeldung.com/project-jigsaw-java-modularity
    - **module:** the module definition file starts with this keyword followed by its name and definition
    - **requires:** is used to indicate the modules it depends on; a module name has to be specified after this keyword
    - **transitive:** is specified after the requires keyword; this means that any module that depends on the module defining requires transitive <modulename> gets an implicit dependence on the <modulename>
    - **exports:** is used to indicate the packages within the module available publicly; a package name has to be specified after this keyword
    - **opens:** is used to indicate the packages that are accessible only at runtime and also available for introspection via Reflection APIs; this is quite significant to libraries like Spring and Hibernate, highly rely on Reflection APIs; opens can also be used at the module level in which case the entire module is accessible at runtime
    - **uses:** is used to indicate the service interface that this module is using; a type name, i.e., complete class/interface name, has to specified after this keyword
    - **provides … with ...:** they are used to indicate that it provides implementations, identified after the with keyword, for the service interface identified after the provides keyword
  - http://files.zeroturnaround.com/pdf/RebelLabs-Java-9-modules-cheat-sheet.pdf