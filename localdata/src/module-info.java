module examples.data.people {
    requires java.base;
    requires transitive examples.data;

    provides examples.data.Data with examples.data.people.NameGenerator;
    exports examples.data.people;
}