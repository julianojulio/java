package examples.data.people;

import examples.data.Data;

import java.util.List;
import java.util.Random;

public class NameGenerator implements Data {

    @Override
    public String value() {
        final List<String> names = List.of("maria", "anna", "cammy");

        final Random random = new Random();
        return names.get(random.nextInt(names.size()));
    }
}
